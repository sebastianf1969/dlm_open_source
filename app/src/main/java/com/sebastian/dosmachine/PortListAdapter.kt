package com.sebastian.dosmachine

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

class PortListAdapter(ports_connected: List<ScanActivity.PortType>) : RecyclerView.Adapter<PortListViewHolder>() {
    var dataSet = ports_connected
    override fun onCreateViewHolder(parent: ViewGroup , p1: Int): PortListViewHolder {
        var view = LayoutInflater.from(parent.context).inflate(R.layout.scan_holder, parent, false)
        return PortListViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: PortListViewHolder, position: Int) {
        viewHolder.bindPort(dataSet.get(position))
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

}

/**
 * Created by Sebastian Florez on 3/12/2016.
 */
