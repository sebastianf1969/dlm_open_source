package com.sebastian.dosmachine

import android.content.Intent
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.View
import com.squareup.picasso.Picasso
import org.jetbrains.anko.find

/**
 * Created by Sebastian Florez on 3/15/2016.
 */
class ServiceListViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) , View.OnClickListener {
    var activator : Intent
    var title : AppCompatTextView
    var subtitle : AppCompatTextView
    var image : AppCompatImageView

    init{
        activator = Intent(itemView.context, LandingActivity::class.java)
        title = itemView.find<AppCompatTextView>(R.id.text_Title)
        subtitle = itemView.find<AppCompatTextView>(R.id.text_Subtitle)
        image = itemView.find<AppCompatImageView>(R.id.imageview_service)
        itemView.setOnClickListener(this)
    }

    fun bindService(service : LandingActivity.ServiceType){
        this.activator = service.activator
        title.text = service.title
        subtitle.text = service.subtitle
        Picasso.with(itemView.context)
        .load(service.image)
        .into(image)
    }

    override fun onClick(p0: View?) {
        itemView.context.startActivity(activator)
    }

}