package com.sebastian.dosmachine

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

/**
 * Created by Sebastian Florez on 3/15/2016.
 */
class ServiceListAdapter(services : List<LandingActivity.ServiceType>) : RecyclerView.Adapter<ServiceListViewHolder>() {
    var dataset = services;
    override fun onCreateViewHolder(parent: ViewGroup?, p1: Int): ServiceListViewHolder? {
        var view = LayoutInflater.from(parent!!.context).inflate(R.layout.service_holder, parent, false)
        return ServiceListViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ServiceListViewHolder?, position: Int) {
        viewHolder!!.bindService(dataset.get(position))
    }

    override fun getItemCount(): Int {
        return dataset.size
    }

}