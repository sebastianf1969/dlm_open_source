package com.sebastian.dosmachine

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.EditText
import org.jetbrains.anko.*
import java.io.IOException
import java.net.*

class ScanActivity : AppCompatActivity(), AnkoLogger {

    data class PortType(val port: Int, val protocol: String, val open: Boolean)

    val ports = mapOf( 20 to "FTP",
            Pair(21, "FTP"),
            Pair(22,  "SSH"),
            Pair(23,  "Telnet"),
            Pair(25,  "SMTP"),
            Pair(53,  "DNS"),
            Pair(69,  "TFTP"),
            Pair(80,  "HTTP"),
            Pair(88,  "Kerberos"),
            Pair(107, "Remote Telnet Service"),
            Pair(115, "SFTP"),
            Pair(139, "netbios-ss"),
            Pair(156, "SQL"),
            Pair(280, "HTTP"),
            Pair(443, "HTTPS"),
            Pair(445, "AD | Samba"),
            Pair(464, "Kerberos Password Util"),
            Pair(514, "Remote Shell"),
            Pair(543, "klogin (Kerberos)"),
            Pair(544, "kshell (Kerberos)"),
            Pair(593, "HTTP (Exchange)"),
            Pair(691, "MS Exchange"),
            Pair(749, "Kerberos Adminn"),
            Pair(752, "kpasswd (Kerberos)"),
            Pair(901, "Samba"),
            Pair(902, "VMSphere"),
            Pair(903, "VMware Remote Console"),
            Pair(944, "NFS Share"),
            Pair(953, "DNS RNDC"),
            Pair(973, "NFS over IPv6"),
            Pair(981, "Remote HTTPS"),
            Pair(992, "Telnet over SSL"),
            Pair(1433, "MSSQL"),
            Pair(1521, "Oracle DB"),
            Pair(2482, "Oracle DB"),
            Pair(2484, "Oracle DB"),
            Pair(3389, "Microsoft Terminal Server"),
            Pair(8080, "HTTP"))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan)

        var ports_connected : List<PortType>
        var scanButton = this.findViewById(R.id.button_scan) as FloatingActionButton
        var hostText = this.findViewById(R.id.ip_editText) as EditText
        var maxPort = this.findViewById(R.id.maxPort_editText) as EditText
        var scan_timeout = find<EditText>(R.id.scantimeoutEditText)

        var recyclerView = this.findViewById(R.id.recycler_port) as RecyclerView
        info("Beginning scan...")

        //TODO: check if there is an internet connection from the device before trying to scan

        scanButton.setOnClickListener {
            if(hostText.text.toString().length > 0) {
                val curMax = maxPort.text.toString()
                //took me to two hours to figure out my phone was pasting a space at the end and that makes it break 10/10 i am the best. I should probably go to sleep.
                val host = hostText.text.toString().trim()
                var timeout = 100
                var maximum = 8049
                if (curMax.length > 0) {
                    maximum = curMax.toInt()
                }
                if(scan_timeout.text.length > 0){
                    timeout = scan_timeout.text.toString().toInt()
                }

                var progress = progressDialog("Please wait a moment.", "Scanning...")
                progress.setCanceledOnTouchOutside(false)
                progress.setCancelable(false)
                val size = maximum
                progress.max = size
                progress.show()
                async() {
                    var current = 1
                    ports_connected = Array(size, {
                        info("Scanning port $current on $host")
                        var con = true
                        try {
                            //TODO: move this back out of the try once connection check is made.
                            var address = InetAddress.getByName(host)
                            val socket = Socket();
                            socket.connect(InetSocketAddress(address.hostAddress, current), timeout)
                            info("\tConnected!")
                        } catch(e: IOException) {
                            con = false;
                        }
                        val name = if (ports.containsKey(current)) ports[current].toString() else "Unknown"
                        val current_use = current
                        current++
                        progress.progress += 1
                        PortType(current_use, name, con)
                    }).filter { a -> a.open == true }

                    uiThread {
                        progress.hide()
                        progress.dismiss()
                        recyclerView.layoutManager = LinearLayoutManager(this@ScanActivity)
                        recyclerView.adapter = PortListAdapter(ports_connected)
                    }
                }
            } else {
                alert("Please input a valid host address.\nEx: website.com\nOr: 127.0.0.1", "No Target Specified") {
                    positiveButton("Okay") {
                        // They learned their lesson
                    }
                }.show()
            }

        }
    }
}
