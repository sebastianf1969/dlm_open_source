package com.sebastian.dosmachine

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import org.jetbrains.anko.find
import java.util.*

class LandingActivity : AppCompatActivity() {

    data class ServiceType(var title: String, var subtitle: String, var image: Int, val activator: Intent)

    var serviceData = ArrayList<ServiceType>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_landing)

        serviceData.add(ServiceType("Apache", "Slow HTTP attack using GET/POST requests, specifically targeting Apache's timeout length.", R.drawable.apache_logo, getApacheContext()))
        serviceData.add(ServiceType("NGINX", "Slow HTTP attack using GET requests, in a manner similar to the Apache attack.", R.drawable.nginx_logo2, getNGINXContext()))
        serviceData.add(ServiceType("Node", "Slow HTTP POST attack, which utilizes partial POST bodies, and causes a concurrent file read error in Node.", R.drawable.nodejs_logo, getIIS6Context()))
        serviceData.add(ServiceType("IIS", "Slow HTTP POST attack, similar to IIS6 but must be used on a legitimate POST route.", R.drawable.iis8_logo2, getIIS7Context()))
        serviceData.add(ServiceType("Port Scan", "Perform a port scan of a host IP to identify open services.", R.drawable.port_scan_logo, getPortScanContext()))

        var recyclerView = find<RecyclerView>(R.id.recycler_actions)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = ServiceListAdapter(serviceData.toList())

    }

    fun getApacheContext() : android.content.Intent {
        return Intent(this , ApacheActivity::class.java)
    }

    fun getNGINXContext() : android.content.Intent {
        return Intent(this , NginxActivity::class.java)
    }

    fun getIIS6Context() : android.content.Intent {
        return Intent(this , NodeActivity::class.java)
    }

    fun getIIS7Context() : android.content.Intent {
        return Intent(this , IISPlusActivity::class.java)
    }

    fun getPortScanContext() : android.content.Intent {
        return Intent(this, ScanActivity::class.java)
    }

}
