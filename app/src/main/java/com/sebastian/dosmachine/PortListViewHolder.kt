package com.sebastian.dosmachine

import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.View
import org.jetbrains.anko.*

class PortListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private var portText : AppCompatTextView
    private var portStatusText : AppCompatTextView

    init {
        portText = itemView.find(R.id.port_text);
        portStatusText = itemView.find(R.id.port_status_text)
    }

    fun bindPort(port : ScanActivity.PortType){
        portText.text = port.port.toString()
        portStatusText.text = port.protocol
    }
}
/**
 * Created by Sebastian Florez on 3/12/2016.
 */